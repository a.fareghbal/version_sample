from django.apps import AppConfig


class ShowVersionConfig(AppConfig):
    name = 'show_version'
