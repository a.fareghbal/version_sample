from rest_framework.response import Response
from rest_framework.views import APIView
from version import exit_messages
from version.settings import VERSION


class VersionView(APIView):

    def get(self, request):

        try:

            return Response(VERSION, status=exit_messages.OK['code'])

        except Exception as e:
            print(e)
            return Response(
                    exit_messages.USER_INPUT_IS_NOT_VALID,
                    status=exit_messages.USER_INPUT_IS_NOT_VALID['code']
            )

