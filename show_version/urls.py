from django.conf.urls import url
from .views import VersionView

urlpatterns = [
    url(r'',VersionView.as_view()),
]