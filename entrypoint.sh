#!/bin/bash

shift
uwsgi --uid www-data --gid www-data --plugins=python3 --chdir=/var/www --socket=0.0.0.0:9000 --module=version.wsgi:application --processes=4 --protocol=http