METHOD_NOT_ALLOWED = {
    "status_code": 405,
    "code": 405,
    "message": "Request method not allowed"
}


THE_REQUESTED_OBJECT_NOT_FOUND = {
    "status_code": 404,
    "code": 404,
    "message": "The requested object not found"
}


USER_INPUT_IS_NOT_VALID = {
    "status_code": 400,
    "code": 400,
    "message": "The user request is not valid"
}

USER_INPUT_TYPE_IS_NOT_VALID = {
    "status_code": 400,
    "code": 400,
    "message": "The user notification type is not valid"
}

DUPLICATED_REQUEST = {
    "status_code": 409,
    "code": 409,
    "message": "Duplicated request"
}

YOU_CANNOT_DELETE_THIS_OBJECT_AT_THIS_TIME = {
    "status_code": 403,
    "code": 403,
    "message": "You can't delete this object."
}


YOU_CANNOT_UPDATE_THIS_OBJECT_AT_THIS_TIME = {
    "status_code": 403,
    "code": 403,
    "message": "You can't update this object."
}

YOUR_REQUEST_HAS_BEEN_ACCEPTED = {
    "status_code": 202,
    "code": 202,
    "message": "Your Request has been queued."
}

YOUR_PROFILE_HAS_BEEN_CREATED = {
    "status_code": 201,
    "code": 201,
    "message": "Your Profile has been created."
}

YOUR_TAG_HAS_BEEN_CREATED = {
    "status_code": 201,
    "code": 201,
    "message": "Your Tag has been created."
}

YOUR_RECEPIENT_HAS_BEEN_ADDED = {
    "status_code": 201,
    "code": 201,
    "message": "Your Recepients list has been added."
}

OK = {
    "status_code": 200,
    "code": 200,
    "message": "ok."
}