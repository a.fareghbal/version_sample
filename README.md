# Django/Rest-framework sample
Django/Rest-framework sample consist of following:
  - python3
  - uwsgi
  - docker

# startup
```
docker-compose up --force-recreate -d --build
```


# How to update

```
$ docker build -t registry.gitlab.com/a.fareghbal/version_sample:<tag> .
$ docker push registry.gitlab.com/a.fareghbal/version_sample:<tag>
$ helm upgrade --install -n version-sample --create-namespace version-sample --set image.tag=<tag> ./charts
```



# what is version-sample?
version_sample is a simple djnago poc project designed to be deployed on top of k8s, the aim is to provide a playground to prepare for a production deployment for any modern system.

## how we deployed k8s?
We've used kubespray which is set of production ready ansible-roles widely used, for this poc we have created a multi-master k8s cluster with stacked etcd.

# Todo
- Add k8s deploy job in ci/cd
- complete documents
- complete monitoring stack (grafana and alertmanager)
