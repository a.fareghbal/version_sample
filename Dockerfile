FROM debian:buster-slim 

LABEL maintainer="ali.fareghbal@gmail.com"
ENV _VERSION="0.0.1"
ENV TAG=0.0.1

# Install System requirnment pakcages
RUN apt-get update && apt-get install -y python3 \
	python3-dev \
	python3-setuptools \
	python3-pip \
	libevent-dev \
	libmariadbclient-dev

# Copy requirenments
COPY ["entrypoint.sh", "requirements.txt", "/"]


# Install python packages
RUN pip3 install --no-cache-dir -r requirements.txt

# Copy project files
COPY --chown=www-data:www-data [".", "/var/www"]

WORKDIR "/var/www"

ENTRYPOINT ["/entrypoint.sh"]
